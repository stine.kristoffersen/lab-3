package no.uib.inf101.terminal;

public interface Command {
    
    // Et interface som har metodene run, som tar inn en liste med strings og returnerer en string
    // Og er metode getname som returnerer en string med 
    String run(String[] args);

    String getName();

    
}
