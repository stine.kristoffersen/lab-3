package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    //String[] args = new String[] { "foo", "bar" };
    //String result = command.run(args);
    
    public String run(String[] args) {
        String result = "";
        for (String ele : args) {
            result += ele + " ";
        }
    return result;
    }

    public String getName() {
        return "echo";
    }

}
